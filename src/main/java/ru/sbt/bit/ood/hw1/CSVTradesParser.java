package ru.sbt.bit.ood.hw1;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Vasyukevich Andrey on 10.10.2016.
 */
public class CSVTradesParser implements TradesParser {
    public Collection<Trade> parse(String filename) {
        try {
            Reader in = new FileReader(filename);
            Iterable<CSVRecord> csvRecords = CSVFormat.EXCEL.parse(in);
            Collection<Trade> trades = new ArrayList<Trade>();
            for(CSVRecord csvRecord : csvRecords)
                trades.add(new Trade(csvRecord.toMap()));
            return trades;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("There was an error while parsing CSV file");
        }
    }
}

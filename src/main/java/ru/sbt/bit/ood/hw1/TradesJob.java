package ru.sbt.bit.ood.hw1;

import java.util.Collection;

public class TradesJob {

    private final TradesDAO tradesDAO;
    private final TradesFileDownloader tradesFileDownloader;
    private final TradesParser tradesParser;

    public TradesJob(TradesDAO tradesDAO, TradesFileDownloader tradesFileDownloader, TradesParser tradesParser) {
        this.tradesDAO = tradesDAO;
        this.tradesFileDownloader = tradesFileDownloader;
        this.tradesParser = tradesParser;
    }

    public void run() {
        String filename = tradesFileDownloader.download();
        Collection<Trade> trades = tradesParser.parse(filename);
        updateTrades(trades);
    }

    private void updateTrades(Collection<Trade> trades) {
        tradesDAO.deleteAll();
        for (Trade trade : trades)
            tradesDAO.save(trade);
    }

}

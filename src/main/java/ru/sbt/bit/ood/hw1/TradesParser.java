package ru.sbt.bit.ood.hw1;

import java.util.Collection;

/**
 * Created by Vasyukevich Andrey on 10.10.2016.
 */
public interface TradesParser {
    public Collection<Trade> parse(String filename);
}

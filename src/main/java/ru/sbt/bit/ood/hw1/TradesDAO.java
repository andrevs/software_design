package ru.sbt.bit.ood.hw1;

import java.util.Collection;

/**
 * Created by Vasyukevich Andrey on 24.10.2016.
 */
public interface TradesDAO {
    public Collection<Trade> findAll();

    public void deleteAll();

    public void save(Trade trade);

}

package ru.sbt.bit.ood.hw1;

/**
 * Created by Vasyukevich Andrey on 10.10.2016.
 */
public interface TradesFileDownloader {
    public String download();
}
